# 1.3.0 - released 17/03/2023

- [i18n] Add swedish translation thanks @christianemanuelsson
- [bug] Sometimes app freeze on start
- [bug] Clear queue crash app
- [bug] Sometimes song don't start
- [bug] Release media-hub when futify stop
- [bug] Sometimes song don't auto-play

# 1.2.0 - released 21/03/2022

- [feature] Add podcasts in home and search pages
- [i18n] Updated dutch translation thanks @Vistaus
- [i18n] Updated hungarian translation thanks @Lundrin
- [i18n] Add german translation thanks @Schmuel
- [i18n] Add spanish translation thanks @raptopassion
- [bug] Display section in home page only if there is something to display (no shows = no section)
- [dep] Update to latest spotify client

# 1.1.0 - released 01/03/2022

- [feature] Add a repeat button on the Player view thanks @discoverti
- [feature] Add a contributors section in about page
- [feature] Add translations mecanism
- [i18n] Add french translation
- [i18n] Add hungarian translation thanks @Lundrin
- [i18n] Add dutch translation thanks @Vistaus
- [bug] Avoid duplicate song in "Recently Play" thanks @discoverti
- [bug] Start album and playlist directly in search result (right action)

# 1.0.1 - released 17/02/2022

- [bug] Launch album in home crash app

# 1.0.0 - released 17/02/2022

- First release in open-store.
