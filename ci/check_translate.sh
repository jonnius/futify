#!/usr/bin/env bash

set -ve

clickable script i18n

if [[ "$(git status --untracked-files=all --porcelain)" ]]; then
git status --untracked-files=all --porcelain
git --no-pager diff
echo "Please regenerate translations with `clickable script i18n` and commit them"
exit 1
else
echo "translation is OK"
exit 0
fi
