/*
 * Copyright (C) 2020  Romain Maneschi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12
import Qt.labs.settings 1.0
import QtQuick.Window 2.0

import "pages"
import "services"
import "model"
import "components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'futify.frenchutouch'
    automaticOrientation: true

    Component {
        id: playlistTypeFactory
        PlaylistType {}
    }

    Component {
        id: albumTypeFactory
        AlbumType {}
    }

    Component {
        id: showTypeFactory
        ShowType {}
    }

    AdaptivePageLayout {
        id: pageStack
        anchors.top: parent.top
        anchors.bottom: playerVisu.top
        width: parent.width

        layouts: [
            PageColumnsLayout {
                when: true;//width <= units.gu(120);
                // Just a single column layout
                PageColumn {
                    fillWidth: true
                }

            }

            // PageColumnsLayout {
            //     when: width > units.gu(120)
            //     PageColumn {
            //         fillWidth: true
            //     }
            //     PageColumn {
            //         minimumWidth: 0
            //         maximumWidth: units.gu(80)
            //         preferredWidth: units.gu(60)
            //     }
            // }
        ]

        onColumnsChanged: {
            if (pageStack.columns > 1) {
                homePage.pageStack.addPageToNextColumn(homePage, queue);
            } else {
                homePage.pageStack.removePages(queue);
            }
        }
    
        Login {
            id: loginPage
            visible: false
            onLogin: function(username, password) {
                error = spotSession.login(username, password);
                if (error === "") {
                    pageStack.primaryPage = homePage;
                    homePage.init();
                } else {
                    loginPage.error = error;
                }
            }
        }

        Home {
            id: homePage
            onNavigate: function(pageName, pageParams, o3, o4, o5) {
                if (pageName === "playlist") {
                    var p = spotSession.getPlaylist(spotSession.loadPlaylist(pageParams.uuid));
                    var newPlaylist = playlistTypeFactory.createObject(playlistPage, p);
                    pageStack.addPageToCurrentColumn(homePage, playlistPage, {playlist: newPlaylist});
                    if (pageStack.columns > 1) {
                        pageStack.addPageToNextColumn(playlistPage, queue);
                    }
                } else if (pageName === "album") {
                    var a = spotSession.getAlbum(spotSession.loadAlbum(pageParams.uuid));
                    var newAlbum = albumTypeFactory.createObject(albumPage, a);
                    pageStack.addPageToCurrentColumn(homePage, albumPage, {album: newAlbum});
                    if (pageStack.columns > 1) {
                        pageStack.addPageToNextColumn(playlistPage, queue);
                    }
                } else if(pageName === "show") {
                    var s = spotSession.getShow(spotSession.loadShow(pageParams.uuid));
                    var newShow = showTypeFactory.createObject(showPage, s);
                    pageStack.addPageToCurrentColumn(homePage, showPage, {show: newShow});
                } else if(pageName === "search") {
                    homePage.pageStack.addPageToCurrentColumn(homePage, searchPage, pageParams);
                    searchPage.changes();
                } else if(pageName === "settings") {
                    homePage.pageStack.addPageToCurrentColumn(homePage, settingsPage, pageParams);
                    settingsPage.init();
                } else if(pageName === "about") {
                    homePage.pageStack.addPageToCurrentColumn(homePage, aboutPage, pageParams);
                } else {
                    console.log('page not known', pageName, pageParams, o3, o4, o5);
                }
            }
            onStartTrack: function(track) {
                audioPlayer.appendAndPlayTrack(track);
            }
            onStartPlaylist: function(playlist) {
                var p = spotSession.getPlaylist(spotSession.loadPlaylist(playlist.uuid));
                var newPlaylist = playlistTypeFactory.createObject(homePage, p);
                audioPlayer.startPlaylist(newPlaylist);
            }
            onStartAlbum: function(album) {
                var p = spotSession.getAlbum(spotSession.loadAlbum(album.uuid));
                var newAlbum = albumTypeFactory.createObject(homePage, p);
                audioPlayer.startAlbum(newAlbum);
            }
            onStartShow: function(show) {
                var s = spotSession.getShow(spotSession.loadShow(show.uuid));
                var newShow = showTypeFactory.createObject(homePage, s);
                audioPlayer.startShow(newShow);
            }
        }

        Playlist {
            id: playlistPage
            playlist: PlaylistType {}
            onStartTrack: function(track) {
                audioPlayer.appendAndPlayTrack(track);
            }
            onAddToEndQueue: function(track) {
                audioPlayer.appendTrack(track);
            }
            onStartPlaylist: function(playlist) {
                audioPlayer.startPlaylist(playlist);
            }
        }

        Album {
            id: albumPage
            album: AlbumType {}
            onStartTrack: function(track) {
                audioPlayer.appendAndPlayTrack(track);
            }
            onAddToEndQueue: function(track) {
                audioPlayer.appendTrack(track);
            }
            onStartAlbum: function(album) {
                audioPlayer.startAlbum(album);
            }
        }

        Show {
            id: showPage
            show: ShowType {}
            onStartTrack: function(track) {
                audioPlayer.appendAndPlayTrack(track);
            }
            onAddToEndQueue: function(track) {
                audioPlayer.appendTrack(track);
            }
            onStartShow: function(show) {
                audioPlayer.startPlaylist(show);
            }
        }

        Search {
            id: searchPage
            onStartTrack: function(track) {
                audioPlayer.appendAndPlayTrack(track);
            }
            onAddToEndQueue: function(track) {
                audioPlayer.appendTrack(track);
            }
            onSelectAlbum: function(album) {
                var a = spotSession.getAlbum(spotSession.loadAlbum(album.uuid));
                var newAlbum = albumTypeFactory.createObject(albumPage, a);
                searchPage.pageStack.addPageToCurrentColumn(searchPage, albumPage, {album: newAlbum});
            }
            onStartAlbum: function(album) {
                var a = spotSession.getAlbum(spotSession.loadAlbum(album.uuid));
                var newAlbum = albumTypeFactory.createObject(audioPlayer, a);
                audioPlayer.startAlbum(newAlbum);
            }
            onSelectPlaylist: function(playlist) {
                var p = spotSession.getPlaylist(spotSession.loadPlaylist(playlist.uuid));
                var newPlaylist = playlistTypeFactory.createObject(playlistPage, p);
                searchPage.pageStack.addPageToCurrentColumn(searchPage, playlistPage, {playlist: newPlaylist});
            }
            onStartPlaylist: function(playlist) {
                var p = spotSession.getPlaylist(spotSession.loadPlaylist(playlist.uuid));
                var newPlaylist = playlistTypeFactory.createObject(audioPlayer, p);
                audioPlayer.startPlaylist(newPlaylist);
            }
            onSelectShow: function(show) {
                var s = spotSession.getShow(spotSession.loadShow(show.uuid));
                var newShow = showTypeFactory.createObject(showPage, s);
                pageStack.addPageToCurrentColumn(searchPage, showPage, {show: newShow});
            }
            onStartShow: function(show) {
                var s = spotSession.getShow(spotSession.loadShow(show.uuid));
                var newShow = showTypeFactory.createObject(homePage, s);
                audioPlayer.startShow(newShow);
            }
        }

        Settings {
            id: settingsPage
            onLogout: {
                spotSession.logout()
                pageStack.primaryPage = loginPage;
            }
        }

        About {
            id: aboutPage
        }

        Component.onCompleted: {
            console.log("OnComplete")
            if (spotSession.settings.theme === 0) {
                theme.name = "Ubuntu.Components.Themes.SuruDark"
            } else if(spotSession.settings.theme === 1) {
                theme.name = "Ubuntu.Components.Themes.Ambiance"
            } else {
                theme.name = ""
            }
            if (spotSession.isLogged || spotSession.tryLogin()) {
                pageStack.primaryPage = homePage;
                homePage.init();
                console.log("OnComplete :: GoTo homePage")
            } else {
                pageStack.primaryPage = loginPage;
                console.log("OnComplete :: GoTo LoginPage")
            }

            if (pageStack.columns > 1) {
                homePage.pageStack.addPageToNextColumn(homePage, queue);
            }
        }
    }

    PlayerView {
        id: playerVisu
        visible: player.playlist.itemCount > 0
        width: parent.width
        height: player.playlist.itemCount > 0 ? units.gu(6) : 0
        anchors.bottom: parent.bottom
        player: audioPlayer
        onShowDetails: function() {
            bottomEdge.commit();
        }
    }

    Queue {
        id: queue
        visible: false
        player: audioPlayer
        height: parent.height
        width: parent.width
    }

    UserMetricsHelper {
        player: audioPlayer
    }

    Player {
        id: audioPlayer
    }

    BottomEdge {
        id: bottomEdge
        height: parent.height - units.gu(20)
        preloadContent: true
        hint.status: "Hidden"
        contentComponent: Queue {
            player: audioPlayer
            height: bottomEdge.height
        }
    }

    Connections {
        target: Qt.application

        onAboutToQuit: {
            audioPlayer.clear()
        }
    }

}
