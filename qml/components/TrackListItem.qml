import QtQuick 2.12
import Ubuntu.Components 1.3
import QtGraphicalEffects 1.0

ListItem {
    property var track

    property bool canDelete: false
    property bool activeSimpleClick: false

    signal playTrack(var track)
    signal endQueue(var track)
    signal deleteTrack(var track)

    width: parent.width
    height: units.gu(10)
    Row {
        height: parent.height
        width: parent.width
        padding: units.gu(2)
        spacing: units.gu(2)

        Item {
            id: itemImage
            width: units.gu(6)
            height: units.gu(6)
            anchors.verticalCenter: parent.verticalCenter
            Image {
                id: image
                source: track.image
                width: parent.width
                height: parent.height
                anchors.verticalCenter: parent.verticalCenter
                fillMode: Image.PreserveAspectFit
                visible: false
            }
            OpacityMask {
                anchors.fill: image
                source: image
                width: image.width
                height: image.height
                maskSource: Rectangle {
                    width: image.width
                    height: image.height
                    radius: 5
                    visible: false // this also needs to be invisible or it will cover up the image
                }
            }
        }
        Column {
            anchors.verticalCenter: itemImage.verticalCenter
            width: parent.width - itemImage.width
            Label { text: track.name; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount: track.popularity > -1 ? 1 : 2; textSize: Label.Large }
            Label { text: track.album || track.artist; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount:1; }
            Label { visible: track.popularity > -1; text: qsTr('popularity')+' '+track.popularity+'%'; textSize: Label.Small; }
        }
    }
    leadingActions: ListItemActions {
        actions: [
            Action {
                visible: canDelete
                iconName: "delete"
                text: qsTr("Delete")
                onTriggered: {
                    deleteTrack(track);
                }
            }
        ]
    }
    trailingActions: ListItemActions {
        actions: [
            Action {
                iconName: "media-playback-start"
                text: qsTr("Play")
                onTriggered: {
                    console.log('trackListItem.qml => playTrack', track.name);
                    playTrack(track);
                }
            },
            Action {
                iconName: "add-to-playlist"
                text: qsTr("End Queue")
                onTriggered: {
                    console.log('trackListItem.qml => end queue', track.name);
                    endQueue(track);
                }
            }
        ]
    }
    onClicked: {
        if (activeSimpleClick) {
            console.log('trackListItem.qml => onCLicked', track.name);
            playTrack(track);
        }
    }
}