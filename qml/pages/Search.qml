import QtQuick 2.12
import Ubuntu.Components 1.3

import "../components"

Page {
    id: page
    property string search: ""

    function changes() {
        //TODO add loader
        //force refresh
        tracks.model = 0;
        albums.model = 0;
        playlists.model = 0;
        artists.model = 0;
        episodes.model = 0;
        shows.model = 0;
        if (search !== "") {
            var res = spotSession.search(page.search);
            tracks.model = res.sizeTracks;
            albums.model = res.sizeAlbums;
            playlists.model = res.sizePlaylists;
            artists.model = res.sizeArtists;
            episodes.model = res.sizeEpisodes;
            shows.model = res.sizeShows;
        }
    }

    signal startTrack(var track)
    signal addToEndQueue(var track)
    signal selectAlbum(var album)
    signal startAlbum(var album)
    signal selectPlaylist(var playlist)
    signal startPlaylist(var playlist)
    signal selectShow(var show)
    signal startShow(var show)

    header: PageHeader {
        id: header
        title: qsTr("Search")+": "+page.search
        StyleHints {
            dividerColor: UbuntuColors.green
        }
        extension: Sections {
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.leftMargin: units.gu(2)
            width: header.width - units.gu(2)

            StyleHints {
                selectedSectionColor: UbuntuColors.green
            }

            actions: [
                Action {
                    text: qsTr("Tracks") + " (" + tracks.model + ")"
                    onTriggered: {
                        tracks.visible = true;
                        episodes.visible = false;
                        albums.visible = false;
                        playlists.visible = false;
                        shows.visible = false;
                        artists.visible = false;
                    }
                },
                Action {
                    text: qsTr("Episodes") + " (" + episodes.model + ")"
                    onTriggered: {
                        tracks.visible = false;
                        episodes.visible = true;
                        albums.visible = false;
                        playlists.visible = false;
                        shows.visible = false;
                        artists.visible = false;
                    }
                },
                Action {
                    text: qsTr("Albums") + " (" + albums.model + ")"
                    onTriggered: {
                        tracks.visible = false;
                        episodes.visible = false;
                        albums.visible = true;
                        playlists.visible = false;
                        shows.visible = false;
                        artists.visible = false;
                    }
                },
                Action {
                    text: qsTr("Playlists") + " (" + playlists.model + ")"
                    onTriggered: {
                        tracks.visible = false;
                        episodes.visible = false;
                        albums.visible = false;
                        playlists.visible = true;
                        shows.visible = false;
                        artists.visible = false;
                    }
                },
                Action {
                    text: qsTr("Shows") + " (" + shows.model + ")"
                    onTriggered: {
                        tracks.visible = false;
                        episodes.visible = false;
                        albums.visible = false;
                        playlists.visible = false;
                        shows.visible = true;
                        artists.visible = false;
                    }
                },
                Action {
                    text: qsTr("Artists") + " (" + artists.model + ")"
                    onTriggered: {
                        tracks.visible = false;
                        episodes.visible = false;
                        albums.visible = false;
                        playlists.visible = false;
                        shows.visible = false;
                        artists.visible = true;
                    }
                }
            ]
        }
    }

    ListView {
        id: tracks
        anchors.top: header.bottom
        width: parent.width
        height: parent.height - header.height
        visible: true
        clip: true
        model: 0
        delegate: TrackListItem {
            track: spotSession.getSearchResultTrack(index)
            canDelete: false
            activeSimpleClick: true
            onPlayTrack: startTrack(track)
            onEndQueue: addToEndQueue(track)
        }
    }

    ListView {
        id: episodes
        anchors.top: header.bottom
        width: parent.width
        height: parent.height - header.height
        visible: false
        clip: true
        model: 0
        delegate: TrackListItem {
            track: spotSession.getSearchResultEpisode(index)
            canDelete: false
            activeSimpleClick: true
            onPlayTrack: startTrack(track)
            onEndQueue: addToEndQueue(track)
        }
    }

    Albums {
        id: albums
        anchors.top: header.bottom
        width: parent.width
        height: parent.height - header.height
        visible: false
        clip: true
        size: 0

        getIndex: function(album, index) {
            return spotSession.getSearchResultAlbum(index)
        }

        onSelect: selectAlbum(selected)
        onStart: startAlbum(selected)
    }

    Playlists {
        id: playlists
        anchors.top: header.bottom
        width: parent.width
        height: parent.height - header.height
        visible: false
        clip: true
        size: 0

        getIndex: function(playlist, index) {
            return spotSession.getSearchResultPlaylist(index)
        }

        onSelect: selectPlaylist(selected)
        onStart: startPlaylist(selected)
    }

    Shows {
        id: shows
        anchors.top: header.bottom
        width: parent.width
        height: parent.height - header.height
        visible: false
        clip: true
        size: 0

        getIndex: function(show, index) {
            return spotSession.getSearchResultShow(index)
        }

        onSelect: selectShow(selected)
        onStart: startShow(selected)
    }

    Artists {
        id: artists
        anchors.top: header.bottom
        width: parent.width
        height: parent.height - header.height
        visible: false
        clip: true
        size: 0

        getIndex: function(playlist, index) {
            return spotSession.getSearchResultArtist(index)
        }

    }
}